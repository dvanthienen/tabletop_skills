Tabletop Skills
=========

This is a repository of tabletop task models/skills and application models that use them.

Dependencies
------------

- iTaSC DSL v0.2: https://bitbucket.org/dvanthienen/itasc_dsl
- for execution: orocos-toolchain, rFSM, itasc repositories, robot specific libraries, ROS environment

Setup
-----
See setup iTaSC DSL

Model checking
-------------
To check an application model against the spec
The model should be in file spec syntax: file://<ROS package name>#/<path from ROS pkg to file>/<model file> or  file://<absolute path to file>/<model>

```sh
./check_itasc_model.lua file://tabletop_skills#/apps/pr2cupstacking.lua
```

Deploy the model using Orocos
-----------------------------
To check an iTaSC model against the spec, transform it to executable code and execute it, 
using the standard Orocos taskbrowser:

### ROS Indigo ###
```sh
$ rosrun rttros deployer -lwarning -s `rospack find itasc_dsl`/itasc_deploy_taskbrowser.ops -- file://tabletop_skills#/apps/pr2cupstacking.lua
```

### older ROS versions ###
```sh
$ rosrun ocl deployer-gnulinux -lwarning -s `rospack find itasc_dsl`/itasc_deploy_taskbrowser.ops -- file://tabletop_skills#/apps/pr2cupstacking.lua
```


Acknowledgement
---------------
Dominick Vanthienen
Mechanical Engineering department of K.U.Leuven, Leuven, Belgium.
(c) KU Leuven 2015


The research leading to these results has received funding from 
 the Flemish FWO project G040410N ('Autonome manipulatietaken met een vliegende robot' 'Autonomous manipulation using a flying robot'), as well as 
the European Community's Seventh Framework Programme (FP7/2007-2013) under grant agreement no. FP7-ICT-231940-BRICS (Best Practice in Robotics), 
Rosetta (2008-ICT-230902, Robot control for skilled execution of tasks in natural interaction with humans; based on autonomy, cumulative knowledge and learning), 
and RoboHow (FP7-ICT-288533 RoboHow.Cog). 
